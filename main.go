package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"
)

func main() {
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "line":
			fmt.Printf(generate())
		case "json":
			tick := time.NewTicker(1 * time.Second)
			jtext := make(map[string]string)
			for {
				select {
				case <-tick.C:
					{
						jtext["text"] = generate()
						json.NewEncoder(os.Stdout).Encode(jtext)
					}
				}
			}
		default:
			usage()
		}
	} else {
		usage()
	}
}

func usage() {
	fmt.Printf("moomyip line  <--- for print one line and stop\n")
	fmt.Printf("moomyip json  <--- for print json and loop\n\n")
	os.Exit(0)
}

func generate() string {
	hostname := getHostname()
	essid := getESSID()
	iface, ip4IN := getIP4IN(hostname)
	ip4OUT := getIP4OUT()
	if essid == "" && iface != "" {
		return fmt.Sprintf(" %v: %v (%v)", iface, ip4IN, ip4OUT)
	} else if iface != "" {
		return fmt.Sprintf(" %v - %v: %v (%v)", essid, iface, ip4IN, ip4OUT)
	} else {
		return fmt.Sprintf("⚠ Disconnected")
	}
}

func getESSID() string {
	essid, err := exec.Command("iwgetid", "--raw").Output()
	if err != nil {
		return ""
	}

	return strings.TrimSpace(string(essid))
}

func getHostname() string {
	hostname, err := os.Hostname()
	panicerr(err)
	return hostname
}

func getIP4IN(hostname string) (string, string) {
	ifaces, err := net.Interfaces()
	panicerr(err)

	for _, i := range ifaces {
		addrs, err2 := i.Addrs()
		panicerr(err2)
		for _, addr := range addrs {
			if i.Name == "lo" {
				continue
			}
			return i.Name, addr.String()
		}
	}
	return "", ""
}

func getIP4OUT() string {
	url := "http://ifconfig.me"
	netClient := &http.Client{
		Timeout: time.Second * 1,
	}
	res, err := netClient.Get(url)
	if err != nil {
		return ""
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return ""
	}
	res.Body.Close()
	return string(body)
}

func panicerr(err error) {
	if err != nil {
		panic(err)
	}
}
